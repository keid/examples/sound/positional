{-# LANGUAGE DeriveAnyClass #-}

-- {-# OPTIONS_GHC -fforce-recomp #-}

module Global.Resource.Sound
  ( Collection(..)
  , Sources
  , configs
  , indices
  , names
  ) where

import RIO

import Resource.Collection (Generic1, Generically1(..), enumerate)
import Resource.Opus qualified as Opus
import Resource.Source qualified as Source
import Resource.Static qualified as Static

data Collection a = Collection
  { drum_loop :: a

  , say_front_left  :: a
  , say_front_right :: a
  }
  deriving stock (Show, Functor, Foldable, Traversable, Generic1)
  deriving Applicative via (Generically1 Collection)

{- TODO: a buffer can be shared between multiple sources

Consider multiple drones flying around the player, giggling.
-}
-- type Buffers = Collection AL.Buffer

type Sources = Collection Opus.Source

Static.filePatterns Static.Files "data/sound"

configs :: Collection Opus.Config
configs = Collection
  { drum_loop = Opus.Config
      { gain        = 1.0
      , loopingMode = True
      , byteSource  = Source.File Nothing DRUM_LOOP_OPUS
      }

  , say_front_left = Opus.Config
      { gain        = 1.0
      , loopingMode = False
      , byteSource  = Source.File Nothing SAY_FRONT_LEFT_OPUS
      }
  , say_front_right = Opus.Config
      { gain        = 1.0
      , loopingMode = False
      , byteSource  = Source.File Nothing SAY_FRONT_RIGHT_OPUS
      }
  }

indices :: Collection Int
indices = fmap fst $ enumerate configs

-- XXX: generic
names :: Collection Text
names = Collection
  { drum_loop       = "drum_loop"
  , say_front_left  = "say_front_left"
  , say_front_right = "say_front_right"
  }
