module Stage.Main.Types
  ( Stage
  , FrameResources(..)
  , RunState(..)
  ) where

import RIO

import Geomancy (Vec2, Vec3)

import Engine.Events qualified as Events
import Engine.Worker qualified as Worker
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Set0

import Global.Resource.Sound qualified as Sound
import Stage.Main.Event.Type (Event)

type Stage = Basic.Stage FrameResources RunState

data FrameResources = FrameResources
  { frScene :: Set0.FrameResource '[Set0.Scene]
  }

data RunState = RunState
  { rsSceneP :: Set0.Process

  , rsEvents :: Maybe (Events.Sink Event RunState)

  , rsSounds :: Sound.Sources

  , rsDrumRoll :: Worker.Var Bool
  , rsDrumPos  :: Worker.Var Vec3

  , rsCursorPos :: Worker.Var Vec2
  , rsCursorP   :: Worker.Merge Vec2

  , rsShowFullscreen :: Worker.Var Bool
  }
