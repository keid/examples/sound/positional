module Stage.Main.Setup
  ( stackStage
  , Stage
  , stage
  ) where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Foreign qualified
import Foreign.C qualified
import Geomancy (vec2, vec3, pattern WithVec2)
import RIO.State (gets, modify')
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk

import Engine.Camera qualified as Camera
import Engine.Events qualified as Events
import Engine.Events.CursorPos qualified as CursorPos
import Engine.Events.MouseButton qualified as MouseButton
import Engine.Types qualified as Engine
import Engine.Vulkan.Types (Queues)
import Engine.Worker qualified as Worker
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Scene
import Render.ImGui qualified as ImGui
import Resource.Region qualified as Region
import Sound.OpenAL.FFI.AL qualified as AL

import Global.Resource.Sound qualified as Sound
import Stage.Main.Render qualified as Render
import Stage.Main.Types (FrameResources(..), RunState(..), Stage)
import Stage.Main.Event.Key qualified as Key
import Stage.Main.Event.MouseButton qualified as MouseButton
import Stage.Main.Event.Sink (handleEvent)
import Engine.Sound.Device qualified as SoundDevice
import Resource.Opus qualified as Opus

stackStage :: Engine.StackStage
stackStage = Engine.StackStage stage

stage :: Stage
stage = Engine.Stage
  { sTitle = "Main"

  , sAllocateRP = Basic.allocate_
  , sAllocateP  = allocatePipelines
  , sInitialRS  = initialRunState
  , sInitialRR  = intialRecyclableResources
  , sBeforeLoop = beforeLoop

  , sUpdateBuffers  = Render.updateBuffers
  , sRecordCommands = Render.recordCommands

  , sAfterLoop    = afterLoop
  }
  where
    allocatePipelines swapchain rps = do
      void $! ImGui.allocate swapchain (Basic.rpForwardMsaa rps) 0
      Basic.allocatePipelines_ swapchain rps

    beforeLoop = do
      cursorWindow <- gets rsCursorPos
      cursorCentered <- gets rsCursorP

      (key, sink) <- Events.spawn
        handleEvent
        [ CursorPos.callback cursorWindow
        , MouseButton.callback cursorCentered MouseButton.clickHandler
        , Key.callback
        ]

      modify' \rs -> rs
        { rsEvents = Just sink
        }

      ImGui.beforeLoop True

      pure key

    afterLoop key = do
      ImGui.afterLoop
      Resource.release key

initialRunState :: Engine.StageSetupRIO (Resource.ReleaseKey, RunState)
initialRunState = Region.run do
  -- XXX: get the sound waves rolling

  sound <- Region.local SoundDevice.allocate
  rsSounds <- traverse (Opus.load sound) Sound.configs

  -- liftIO $ AL.alDistanceModel AL.INVERSE_DISTANCE_CLAMPED

  liftIO do
    AL.alSourcei (snd $ Sound.say_front_left rsSounds) AL.SOURCE_RELATIVE (Foreign.C.CInt 1)
    Foreign.with (vec3 (-1) 0 1) $
      AL.alSourcefv (snd $ Sound.say_front_left rsSounds) AL.POSITION . Foreign.castPtr

  liftIO do
    AL.alSourcei (snd $ Sound.say_front_right rsSounds) AL.SOURCE_RELATIVE (Foreign.C.CInt 1)
    Foreign.with (vec3 1 0 1) $
      AL.alSourcefv (snd $ Sound.say_front_right rsSounds) AL.POSITION . Foreign.castPtr

  liftIO do
    AL.alSourcei (snd $ Sound.drum_loop rsSounds) AL.SOURCE_RELATIVE (Foreign.C.CInt 0)
    Foreign.with (vec3 0 1 1) $
      AL.alSourcefv (snd $ Sound.drum_loop rsSounds) AL.POSITION . Foreign.castPtr
    Foreign.with (Foreign.C.CFloat 1.0) $
      AL.alSourcefv (snd $ Sound.drum_loop rsSounds) AL.ROLLOFF_FACTOR . Foreign.castPtr

  rsDrumRoll <- Worker.newVar False
  rsDrumPos <- Worker.newVar $ vec3 0 1 1

  -- XXX: continue with the graphics stuff

  screen <- Engine.askScreenVar
  rsCursorPos <- Worker.newVar 0
  rsCursorP <-
    Worker.spawnMerge2
      (\Vk.Extent2D{width, height} (WithVec2 windowX windowY) ->
          vec2
            (windowX - fromIntegral width / 2)
            (windowY - fromIntegral height / 2)
      )
      screen
      rsCursorPos

  ortho <- Camera.spawnOrthoPixelsCentered
  rsSceneP <- Worker.spawnMerge1 mkScene ortho

  let rsEvents = Nothing
  rsShowFullscreen <- Worker.newVar False

  pure RunState{..}
  where
    mkScene Camera.Projection{..} = Scene.emptyScene
      { Scene.sceneProjection = projectionTransform
      }

intialRecyclableResources
  :: Queues Vk.CommandPool
  -> Basic.RenderPasses
  -> Basic.Pipelines
  -> ResourceT (Engine.StageRIO RunState) FrameResources
intialRecyclableResources _cmdPools _renderPasses pipelines = do
  frScene <- Scene.allocateEmpty (Basic.getSceneLayout pipelines)

  pure FrameResources{..}
