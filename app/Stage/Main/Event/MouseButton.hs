module Stage.Main.Event.MouseButton
  ( clickHandler
  ) where

import RIO

import Engine.Events.MouseButton (ClickHandler)
import Engine.Events.Sink (MonadSink, Sink(..))
import Render.ImGui qualified as ImGui
-- import Engine.UI.Layout qualified as Layout
-- import Engine.Window.MouseButton (MouseButton(..), MouseButtonState(..))

import Stage.Main.Event.Type (Event)
import Stage.Main.Event.Type qualified as Event
import Stage.Main.Types (RunState(..))

clickHandler :: MonadSink RunState m => ClickHandler Event RunState m
clickHandler (Sink signal) cursorPos buttonEvent =
  ImGui.capturingMouse do
    logInfo $ "Click at: " <> displayShow cursorPos <> " " <> displayShow buttonEvent
    signal Event.DoNothing
