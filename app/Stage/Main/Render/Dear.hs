{-# LANGUAGE OverloadedLists #-}

module Stage.Main.Render.Dear where

import RIO

import Data.StateVar (makeStateVar)
import Data.Text qualified as Text (pack)
import DearImGui qualified
import DearImGui.Raw qualified
import Foreign qualified
import Foreign.C qualified as C
import RIO.State (gets)
import Data.List qualified as List

import Engine.Sound.Source qualified as Source
import Engine.Types (StageFrameRIO)
import Engine.Worker qualified as Worker
import Geomancy.Vec3 qualified as Vec3
import Render.ImGui qualified as ImGui
import Sound.OpenAL.FFI.AL qualified as AL

import Global.Resource.Sound qualified as Sound
-- import Stage.Main.Event.Type qualified as Event
import Stage.Main.Types (RunState(..))

imguiDrawData :: StageFrameRIO rp p fr RunState DearImGui.DrawData
imguiDrawData = fmap snd $ ImGui.mkDrawData do
  sounds@Sound.Collection{..} <- gets rsSounds

  DearImGui.button "front left" >>= \clicked ->
    when clicked $
      Source.play1 say_front_left

  DearImGui.button "front right" >>= \clicked ->
    when clicked $
      Source.play1 say_front_right

  drumRoll <- gets rsDrumRoll
  _checked <- DearImGui.checkbox "drum roll" $
    makeStateVar
      (Worker.getInputData drumRoll)
      (\checked -> do
          Worker.pushInput drumRoll $ const checked
          if checked then
            Source.play1 drum_loop
          else
            Source.stop drum_loop
      )

  DearImGui.separator

  let showT :: Show a => a -> Text
      showT = Text.pack . show
  for_ ((,) <$> Sound.names <*> sounds) \(name, (duration, source)) ->
    DearImGui.withID (showT name) do
      DearImGui.text $ showT name
      DearImGui.text $ "Duration: " <> (showT duration)

      liftIO $ Foreign.alloca \ptr -> do
        AL.alGetSourcefv source AL.GAIN ptr
        changed <- C.withCString "Gain" \label ->
          DearImGui.Raw.sliderFloat label ptr 0 1 Foreign.nullPtr
        when changed $
          AL.alSourcefv source AL.GAIN ptr

      liftIO $ Foreign.alloca \ptr -> do
        AL.alGetSourcefv source AL.PITCH ptr
        changed <- C.withCString "Pitch" \label ->
          DearImGui.Raw.sliderFloat label ptr (1/8) 8.0 Foreign.nullPtr
        when changed $
          AL.alSourcefv source AL.PITCH ptr

      liftIO $ Foreign.alloca \ptr -> do
        AL.alGetSourcefv source AL.ROLLOFF_FACTOR ptr
        changed <- C.withCString "Rolloff factor" \label ->
          DearImGui.Raw.sliderFloat label ptr 0.05 20 Foreign.nullPtr
        when changed $
          AL.alSourcefv source AL.ROLLOFF_FACTOR ptr

      liftIO $ Foreign.alloca \ptr -> do
        AL.alGetSourcefv source AL.REFERENCE_DISTANCE ptr
        changed <- C.withCString "Reference distance" \label ->
          DearImGui.Raw.sliderFloat label ptr (1/256) 32.0 Foreign.nullPtr
        when changed $
          AL.alSourcefv source AL.REFERENCE_DISTANCE ptr

      DearImGui.separator

  drumPos <- gets rsDrumPos
  let
    posVar = makeStateVar
      (fmap (`Vec3.withVec3` (,,)) $ Worker.getInputData drumPos)
      (\(Vec3.fromTuple -> new) -> do
          Worker.pushInput drumPos $ const new
          Foreign.with new $
            AL.alSourcefv (snd drum_loop) AL.POSITION . Foreign.castPtr
      )
  _pos <- DearImGui.dragFloat3 "Pos" posVar 0.05 (-10) 50

  -- TODO: source-relative

  liftIO $ Foreign.alloca \modelPtr -> do
    let
      modelVar = makeStateVar
        do
          AL.alGetIntegerv AL.DISTANCE_MODEL modelPtr
          model <- Foreign.peek modelPtr
          pure . fromMaybe 0 $
            List.findIndex
              ((== model) . fst)
              distanceModels

        ( \selected ->
            AL.alDistanceModel $
              fst $ distanceModels List.!! selected
        )

    void $! DearImGui.combo "Distance model" modelVar $
      map (Text.pack . snd) distanceModels

  pure ()

distanceModels :: [(C.CInt, String)]
distanceModels =
  [ (0, "none")
  , (AL.INVERSE_DISTANCE, "inverse")
  , (AL.INVERSE_DISTANCE_CLAMPED, "inverse clamped")
  , (AL.LINEAR_DISTANCE, "linear")
  , (AL.LINEAR_DISTANCE_CLAMPED, "linear clamped")
  , (AL.EXPONENT_DISTANCE, "exponent")
  , (AL.EXPONENT_DISTANCE_CLAMPED, "exponent clamped")
  ]
