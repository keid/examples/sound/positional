module Stage.Main.Event.Sink
  ( handleEvent
  ) where

import RIO

import RIO.State (gets)

import Engine.Types (StageRIO)
import Engine.Worker qualified as Worker

import Stage.Main.Event.Type (Event(..))
import Stage.Main.Types (RunState(..))

handleEvent :: Event -> StageRIO RunState ()
handleEvent = \case
  DoNothing ->
    logInfo "Busy doing nothing."

  ToggleFullscreen -> do
    showFullscreen <- gets rsShowFullscreen
    Worker.pushInput showFullscreen not
